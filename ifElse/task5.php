<?php
function isElsetask5Loader($a){
	
	if (isNumber($a)){
		$result = isElseTask5($a);
		return $result;
	}
	return false;
}
function isElseTask5($rating) {
	
	$result = "";
	if ($rating < 20){
		$result = "F";
	}	
	else if ($rating < 40){
		$result = "E";
	}
	else if ($rating < 60){
		$result = "D";
	}
	else if ($rating < 75){
		$result = "C";
	}
	else if ($rating < 90){
		$result = "B";
	}
	else{
		$result = "A";
	}
	return $result; 	
}
function isNumber($num){
	if (is_numeric($num) && $num >= 0 && $num <= 100) {
		return true;
	}
	return false;
}

echo isElsetask5Loader(60);
?>