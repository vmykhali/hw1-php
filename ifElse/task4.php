<?php

function isNumb($num1, $num2, $num3){
	if (is_numeric($num1) && is_numeric($num2) && is_numeric($num3)){
		return true;
	}
	return false;
}
function isElsetask4Loader($a, $b, $c){
	if (isNumb($a, $b, $c)){
		$result = isElseTask4($a, $b, $c);
		return $result;
	}
	return false;
}
function isElseTask4($a, $b, $c) {
	$num1 = $a * $b * $c;
	$num2 = $a + $b + $c;
	return max($num1, $num2) + 3;
}


echo isElsetask4Loader(3, 1, 2);
?>