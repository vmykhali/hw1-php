<?php
function isElseTask1($a, $b) {
	if (is_numeric($a) && is_numeric($b)) {
		$result = "";
		$mainResult = 0;

		if ($a % 2 == 0){
			$mainResult = $a * $b;
			settype($mainResult, "string");
			$result = 'first_task (a * b) = ' .$mainResult;
		}
		else{
			$mainResult = $a + $b;
			settype($mainResult, "string");
			$result = "first_task (a + b) = " .$mainResult;
		}
		return $mainResult;
	}
	return $undefined;
}

echo isElseTask1(2, 3);
?>
