<?php

function isElsetask2Loader($x, $y) {
	if (is_numeric($x) && is_numeric($y)) {
		$result = isElseTask2($x , $y);
		return $result;
	}
	return false;
}
function isElseTask2($x , $y){
	$result = "";
	if ($x > 0 && $y > 0){
		$result = "part 1";
	}
	else if ($x < 0 && $y > 0){
		$result = "part 2";
	}
	else if ($x < 0 && $y < 0){
		$result = "part 3";
	}	
	else if ($x > 0 && $y < 0){
		$result = "part 4";
	}
	return $result;
}

echo isElsetask2Loader(1, -2);
?>