<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>PHP hw1</title>
</head>
<body>
<?php 
   echo "<h3>if else</h3>";
   echo '<br><a href="./ifElse/task1.php">task1</a>';
   echo '<br><a href="./ifElse/task2.php">task2</a>';
   echo '<br><a href="./ifElse/task3.php">task3</a>';
   echo '<br><a href="./ifElse/task4.php">task4</a>';
   echo '<br><a href="./ifElse/task5.php">task5</a>';
?>
<hr>
<?php 
   echo "<h3>loops</h3>";
   echo '<br><a href="./loops/task1.php">task1</a>';
   echo '<br><a href="./loops/task2.php">task2</a>';
   echo '<br><a href="./loops/task3.php">task3</a>';
   echo '<br><a href="./loops/task4.php">task4</a>';
   echo '<br><a href="./loops/task5.php">task5</a>';
   echo '<br><a href="./loops/task6.php">task6</a>';
?>
<hr>
<?php 
   echo "<h3>arrays</h3>";
   echo '<br><a href="./arrays/task1.php">task1</a>';
   echo '<br><a href="./arrays/task2.php">task2</a>';
   echo '<br><a href="./arrays/task3.php">task3</a>';
   echo '<br><a href="./arrays/task4.php">task4</a>';
   echo '<br><a href="./arrays/task5.php">task5</a>';
   echo '<br><a href="./arrays/task6.php">task6</a>';
   echo '<br><a href="./arrays/task7.php">task7</a>';
   echo '<br><a href="./arrays/task8.php">task8</a>';
   echo '<br><a href="./arrays/task9.php">task9</a>';
?>
<hr>
<?php 
   echo "<h3>functions</h3>";
   echo '<br><a href="./functions/task1.php">task1</a>';
   echo '<br><a href="./functions/task2.php">task2</a>';
   echo '<br><a href="./functions/task3.php">task3</a>';
   echo '<br><a href="./functions/task4.php">task4</a>';
?>
<hr>
</body>
</html>