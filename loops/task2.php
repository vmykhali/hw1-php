<?php
function task2Loader($num){
	if (checkTask2($num)){
		$result = loopsTask2($num);
		return $result;
	}
	return false;
}
function loopsTask2($num) {
	$isSimple = true;
	
	if ($num > 0 && $num < 3)
		$isSimple = true;
	for ($i = 2; $i < $num; $i++) {
		if ($num % $i == 0) {
			$isSimple = false;
		}
	}
	$res = "";
	
	switch($isSimple)
	{
		case true:
			$res = "is a simple number";
			break;
		case false:
			$res = "is a composite number";
	}
	return $res;
}
function checkTask2($num){
	if (is_numeric($num) && $num > 0) {
		return true;
	}
	return false;
}

echo task2Loader(30);
?>