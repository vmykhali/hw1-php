<?php
function task3Loader($num){
	if (checkTask3($num)){
		$result = loopsTask3($num);
		return $result;
	}
	return false;
}
function task3_1Loader($num){
	if (checkTask3($num)){
		$result = loopsTask3_1($num);
		return $result;
	}
	return false;
}

function loopsTask3($num) {

    for ($a = 1.0; $a < $num; $a += 0.1){
        if ($a * $a == $num){
            return $a;
        }
        else if($a * $a > $num){
            return round($a - 0.1);
        }
    }
}


function loopsTask3_1($num) {

    $max = $num;
    $min = 1;
    if ($num < 3){
        return $min;
    }
    while ($max - $min > 1){
        
        $mid = floor(($max + $min) / 2);
        if ($mid * $mid <= $num) {
            $min = $mid;
        }
        else{
            $max = $mid;
        }
    }
    return $min;
}
function checkTask3($num){
	if (is_numeric($num) && $num > 0) {
		return true;
	}
	return false;
}

echo task3Loader(121);
?><br>
<?php
echo task3_1Loader(414);
?>