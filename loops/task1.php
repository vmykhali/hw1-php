<?php

function task1Loader($low = 1, $hi = 100){
	if (checkTask1($low, $hi)){
		$result = loopsTask1($low, $hi);
		return $result;
	}
	return false;
}
function loopsTask1($low, $hi) {
	$evenSum = 0;
	for ($count = $low; $count < $hi; $count++)
	{
		if ($count % 2 == 0)
			$evenSum++;
	}
	return $evenSum;
}
function checkTask1($low, $hi){
	if (is_numeric($low) && is_numeric($hi)){
		return true;
	}
	return true;
}

echo task1Loader(2, 100);
?>