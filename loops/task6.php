<?php
function task6Loader($num){
	if (checkTask6($num)){
		$result = loopsTask6($num);
		return $result;
	}
	return false;
}
function loopsTask6($num) {
	$result = "";
   	$minus = false;
    
    if ($num < 0){
        $minus = true;
        $num *= -1;
    }
    while($num != 0)
    {
        $result .= round($num % 10);
        $num = floor($num / 10);
    }
    settype($result, "integer");
    if ($minus){
        $result *= -1;
    }
    return $result;
}
function checkTask6($num){
	if (is_numeric($num)) {
		return true;
	}
	return false;
}

echo task6Loader(213);
?>