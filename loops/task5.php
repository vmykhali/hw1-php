<?php
function task5Loader($num){
	if (checkTask5($num)){
		$result = loopsTask5($num);
		return $result;
	}
	return false;
}
function loopsTask5($num) {
	$result  = 0;
	if ($num < 0){
		$num = $num *(-1);
	}
   	while($num != 0)
    {
		$result += floor($num % 10);
        $num = floor($num / 10);
    }        
    return $result;
}
function checkTask5($num){
	if (is_numeric($num)) {
		return true;
	}
	return false;
}

echo task5Loader(31);
?>