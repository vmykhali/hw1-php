<?php
function task4Loader($num){
	if (checkTask4($num)){
		$result = loopsTask4($num);
		return $result;
	}
	return false;
}
function loopsTask4($num) {

    $res = 1;
    for ($i = 1; $i <= $num; $i++)
        $res *= $i;
    return $res;
}
function checkTask4($num){
	if (is_numeric($num) && $num >= 0) {
		return true;
	}
	return false;
}

echo task4Loader(4);
?>