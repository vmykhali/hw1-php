<?php
function arrayTask3Loader($inputArray){
    if (is_array($inputArray)){
        return arraytask3($inputArray);
    }
    return null;
}
function arraytask3($inputArray) {
    $min = $inputArray[0];
    $count = 0;
    for ($i = 1; $i < count($inputArray); $i++){
        if ($min > $inputArray[$i]){
            $min = $inputArray[$i];
            $count = $i;
        }
    }
    return $count;
}

echo arrayTask3Loader([22, 21, 23, 4, 5, 1, 9]);
?>