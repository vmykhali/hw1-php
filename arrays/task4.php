<?php
function arrayTask4Loader($inputArray){
    if (is_array($inputArray)){
        return arraytask4($inputArray);
    }
    return null;
}
function arraytask4($inputArray) {
    $max = $inputArray[0];
    $count = 0;
    for ($i = 1; $i < count($inputArray); $i++){
        if ($max < $inputArray[$i])
        {
            $max = $inputArray[$i];
            $count = $i;
        }
    }
    return $count;
}

echo arrayTask4Loader([22, 21, 23, 4, 5, 1, 9]);
?>