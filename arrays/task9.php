<?php
function arrayTask9_1Loader($inputArray){
    if (is_array($inputArray)){
        return arraytask9_1($inputArray);
    }
    return null;
}
function arrayTask9_2Loader($inputArray){
    if (is_array($inputArray)){
        return arraytask9_2($inputArray);
    }
    return null;
}
function arrayTask9_3Loader($inputArray){
    if (is_array($inputArray)){
        return task9_3($inputArray);
    }
    return null;
}

function arraytask9_1($inputArray){
    $length = count($inputArray);
    for ($i = 0; $i < $length - 1; $i++){
        for ($j = $i + 1; $j < $length; $j++)
        {
            if ($inputArray[$i] > $inputArray[$j])
            {
                $temp = $inputArray[$i];
                $inputArray[$i] = $inputArray[$j];
                $inputArray[$j] = $temp;
            }
        }
    }
    return $inputArray;
}
   // сортировка выбором
function arraytask9_2($inputArray){
    for ($currIndex = 0; $currIndex < count($inputArray); $currIndex++)
    {
        $index = Find_Min($inputArray, $currIndex);                // ищем индекс элем. с мин. значением
        /*свап*/
        $temp = $inputArray[$currIndex];
        $inputArray[$currIndex] = $inputArray[$index];
        $inputArray[$index] = $temp;
    }
    return $inputArray;
}


// Сортировка вставками
function task9_3($inputArray){
    for ($i = 1; $i < count($inputArray); $i++)
        for ($j = $i; $j > 0 && $inputArray[$j - 1] > $inputArray[$j]; $j--)
        {
            $temp = $inputArray[$j - 1];
            $inputArray[$j - 1] = $inputArray[$j];
            $inputArray[$j] = $temp;
        }
    return $inputArray;
}

/// для поиска найменьшего значения в массиве
function Find_Min($inputArray, $currIndex){   
    $min = $inputArray[$currIndex];        
    $count = 0;
    for ($i = $currIndex; $i < count($inputArray); $i++)
        if ($min >= $inputArray[$i])
        {
            $min = $inputArray[$i];
            $count = $i;
        }
    return $count;
}

print_r (arrayTask9_1Loader([22, 3, 4, 5, 1, 5, 33, 4, -2 , 2]));?><br>
<?php
print_r (arrayTask9_2Loader([22, 3, 4, 5, 1, 5, 33, 4, -2 , 2]));?><br>
<?php
print_r (arrayTask9_3Loader([22, 3, 4, 5, 1, 5, 33, 4, -2 , 2]));?>