<?php
function arrayTask5Loader($inputArray){
    if (is_array($inputArray)){
        return arraytask5($inputArray);
    }
    return null;
}
function arraytask5($inputArray) {
    $sum = 0;
    for ($i = 0; $i < count($inputArray); $i++) {
        if ($i % 2 == 1) {
            $sum += $inputArray[$i];
        }
    }
    return $sum;
}

echo arrayTask5Loader([22, 3, 1, 44, 5, 66, 2]);
?>