<?php
function arrayTask2Loader($inputArray){
    if (is_array($inputArray)){
        return arraytask2($inputArray);
    }
    return null;
}

function arraytask2($inputArray) {
    $max = $inputArray[0];
    for ($i = 1; $i < count($inputArray); $i++)
        if ($max < $inputArray[$i])
            $max = $inputArray[$i];
    return $max;
}

echo arrayTask2Loader([22, 3, 1, 4, -1]);
?>
