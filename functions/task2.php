<?php

function funTask2Loader($num) {
	if (is_numeric($num) && $num > -1 && $num < 1000)
	{
		return numberString($num);
	}
	else
	{
		return "Error: isn't valid insert of number";
	}
	
}
function numberString($num) {
	$tempString = "";
	$tenVar = floor($num / 10); 	
	$number = [
				"zero", //0 
                "one",  //1 
                "two",  //2
                "three", //3
                "four",  //4
                "five",  //5
                "six",  //6
                "seven", //7
                "eight", //8
                "nine", //9
                "ten", //10
                "eleven",  //11
                "twelve", //12
                "thir", //30, 13 
                "teen", // 13 - 19 14
                ];
    $number_p = array();
    $number_p[7] = "twen";
    $number_p[1] = "eigh";
    $number_p[6] = "thir"; 

    $num = round($num);
    if ($num > 0 && $num < 1000)
    {
    	if (floor($num / 100) != 0)
    	{
    		$tempString = $number[floor($num/100)] . " " . "hundred";
    		$tempString .= ($num % 100) != 0 ?  " " . numberString($num % 100) : "";
    		return $tempString;
    	}
    	else if ($num < 13)
    		return $number[$num];
    	else if ($num == 13)
    		return $number[13] + $number[14];
    	else if ($num == 18)
    		return $number_p[1] + $number[14];
    	else if ($num > 13 && $num < 20)
    		return $number[$num - 10] +  $number[14];
    	else if ($tenVar == 2 || $tenVar == 3 || $tenVar == 8)
    	{
    		$tempString = $number_p[9 - $tenVar] . "ty";
    		$tempString .= ($num % 10) != 0 ?  " " . numberString($num % 10) : "";
    		return $tempString;
    	}
    	else
    	{
    		$tempString = $number[floor($num/10)] . "ty";
    		$tempString .= ($num % 10) != 0 ?  " " . numberString($num % 10) : "";
    		return $tempString;
    	}
    }
    else if ($num == 0)
    	return $number[0];
}

echo funTask2Loader(401);
?>