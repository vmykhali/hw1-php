<?php
// /AB = sqrt((xb - xa)(xb - xa) + (yb - ya)(yb - ya))
function functionTask4($x1, $y1, $x2, $y2) {
    return round(sqrt(($x2 - $x1)*($x2 - $x1) + ($y2 - $y1)*($y2 - $y1)), 2);
}

function funTask4Loader($x1, $y1, $x2, $y2) {
	if (is_numeric($x1) && is_numeric($x2) && is_numeric($y1) && is_numeric($y2)) {
		return functionTask4($x1, $y1, $x2, $y2);
	}
	return "Error: invalid input";
}

echo funTask4Loader(2, 4, 9 , 3);
?>