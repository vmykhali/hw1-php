<?php
function funTask1Loader($num){
	return functionTask1($num);
}
function functionTask1($num) {
	$daysArray = array(
					"Monday",
		            "Tuesday",
		            "Wednesday",
		            "Thursday",
		            "Friday",
		            "Saturday",
		            "Sunday"
	);
    if (is_numeric($num) && $num > 0 && $num <= 7){
		return $daysArray[$num - 1];
	}
    else {
		return "Error: isn't valid insert of number";
	}
}

echo funTask1Loader(4);
?>