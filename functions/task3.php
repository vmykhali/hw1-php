<?php
function funTask3Loader($str) {
	if (is_string($str)) {
		return functionTask3($str); // введите название числа строкой
	}
	return "Error: invalid input";
}

function functionTask3($str) {
	$number = array(
				array("zero"), //0 
                array("one"),  //1 
                array("two", "twelve", "twenty"),  //2 , 12 , 20
                array("three", "thirteen", "thirty"), //3 , 13, 30
                array("four", "fourteen", "fourty"),  //4
                array("five", "fifteen", "fifty"),  //5
                array("six", "sixteen", "sixty"),  //6
                array("seven", "seventeen", "seventy"), //7
                array("eight", "eighteen", "eighty"), //8
                array("nine", "nineteen", "ninety"), //9
                array("ten"), //10
                array("eleven"),  //11
                

				array("hundred","thousand", "million", "billion")
			);
    $array = explode(" ", $str);
    $tempNum = 0;
    $outNum = 0;
    foreach ($array as $str)
    {
    	$checker = false;
    	for ($i = 0; $i < 12; $i++)
    	{
    		if(array_search($str, $number[$i]) !== false)
    		{
    			$index = array_search($str, $number[$i]);
    			if ($index == 0)
    			{
    				$tempNum += $i;
    			}
    			else if ($index == 1)
    				$tempNum += $i + 10;
    			else if ($index == 2)
    				$tempNum += $i * 10;
    		}
    	}
    	if(array_search($str, $number[12]) !== false)
		{
			$index = array_search($str, $number[12]);
			if ($index == 0)
				$tempNum *= 100;
			else if ($index == 1)
			{
				$tempNum *= 1000;
				$checker = true;
			}
			else if ($index == 2 || $index == 3)
				{
					if($index == 2)
						$tempNum *= 1000000;
					else
						$tempNum *= 1000000000;
					$checker = true;
				}
		}
		if ($checker)
		{
			$outNum += $tempNum;
			$tempNum = 0;
		}
    }
    $outNum += $tempNum;
	return $outNum;
}
echo funTask3Loader("four hundred one");
?>